import Login from './screens/Login';    //screen Login
import SignUp from './screens/SignUp';  //screen Signup
import Chat from './screens/Chat';      //screen Chat
import { createStackNavigator } from 'react-navigation'; //import library u/ navigasi

const ScreenManager = createStackNavigator({
  Login: { screen: Login },
  SignUp: { screen: SignUp },
  Chat: { screen: Chat },
});

// Export ScreenManager
export default ScreenManager