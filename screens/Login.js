import React, { Component } from 'react';
import { Constants, ImagePicker, Permissions } from 'expo';
import {
  StyleSheet, Text,
  TextInput,  TouchableOpacity, View,
  Button, ImageEditor, Image,
} from 'react-native';
import firebaseConfig from '../FirebaseConfig'; //import file firebaseConfig
import firebase,{ auth, initializeApp, storage } from 'firebase'; //import functionfirebase 
import uuid from 'uuid';

class Login extends Component {
  static navigationOptions = {
    title: 'Selamat Datang di Aplikasi Chat',
  };

  state = {
    name: 'ab',
    email: 'ab',
    password: 'ab',
    avatar: 'ab',
  };

  // function untuk login 
  onPressLogin = async () => {
    const userData = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      avatar: this.state.avatar,
    };

    const response = firebaseConfig.login(
      userData,
      this.loginSuccess,
      this.loginFailed
    );
  };

  loginSuccess = () => {
    console.log('Login Berhasil');
    this.props.navigation.navigate('Chat', {
      name: this.state.name,
      email: this.state.email,
      avatar: this.state.avatar,
    });
  };

  loginFailed = () => {
    console.log('Login Gagal');
    alert('Login failure. Please tried again.');
  };


  onChangeEmail = text => {
    this.setState({ text })
    };

  onChangePassword = text => {
    this.setState({ text })
    };


  render() {
    return (
      <View>
        <Image style={styles.logoLogin} source={require('../assets/icons/icon-chat.png')}
              resizeMode="contain"/>
        <Text style={styles.title}>Email</Text>
        <TextInput
          style={styles.nameInput}
          placeholder = "Masukan Email"
          placeholderTextColor = "gray"
          autoCapitalize = "none"
          onChangeText={this.onChangeEmail}
          value={this.state.email}
        />
        <Text style={styles.title}>Password</Text>
        <TextInput
          style={styles.nameInput}
          placeholder = "Masukan Password"
          placeholderTextColor = "gray"
          autoCapitalize = "none"
          onChangeText={this.onChangePassword}
          value={this.state.password}
        />

        <TouchableOpacity
          style={styles.button}
          onPress={this.onPressLogin}>
          <Text style={styles.buttonText}> Login </Text>
        </TouchableOpacity>

        <Text 
        style={{alignSelf:"center", marginTop:20, marginBottom:10}}>
        Belum punya akun? 
        </Text>

        <TouchableOpacity
          style={styles.button}
          onPress={() => this.props.navigation.navigate("CreateAccount")}>
          <Text style={styles.buttonText}> Sign Up </Text>
        </TouchableOpacity>

        
      </View>
    );
  }
}

const offset = 16;
const styles = StyleSheet.create({

  title: {
    marginTop: offset,
    marginLeft: offset,
    fontSize: offset,
    fontWeight: 'bold'
  },

  nameInput: {
    height: offset * 2,
    margin: offset,
    paddingHorizontal: offset,
    borderColor: '#111111',
    borderWidth: 1,
    fontSize: offset,
  },

  button: {
    borderWidth:3,
    borderColor:'red',
    flex:1,
    backgroundColor: 'blue',
    margin:15,
    height:40,
    borderRadius:10
  },

  buttonText: {
  color:'black',
  fontSize: 18,
  alignSelf: 'center',
  justifyContent: 'center',
  margin:10
},


  logoLogin:{
    marginTop:20,
    alignSelf:'center',
  }
});

export default Login;
